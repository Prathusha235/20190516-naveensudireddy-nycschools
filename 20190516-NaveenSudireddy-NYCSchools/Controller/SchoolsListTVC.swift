//
//  SchoolsListTVC.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import UIKit

class SchoolsListTVC: UITableViewController {
    
    var schoolsData = [SchoolsData]()
    var satData = [SatResultsData]()
    var filteredSchoolData = [SchoolsData]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add searchcontroller to navigation bar
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search school name here"
        searchController.searchResultsUpdater = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .refresh, target: self, action: #selector(getDataFromAPIServices))
        
        getDataFromAPIServices() //Fetches data from api
        
    }
    
    @objc func getDataFromAPIServices(){
        let apiServices = Services()
        ActivityIndicator.showAnimation(strMsg: "Updating Data..")
        apiServices.getNYSchoolsData{ (json) in
            DispatchQueue.main.async {
                self.schoolsData = json ?? [SchoolsData]()
                self.filteredSchoolData = self.schoolsData //Add master data to filter list
                self.tableView.reloadData()
            }
            apiServices.getSATData(completionHandler: { (json) in // Fetching schools results
                self.satData = json ?? [SatResultsData]()
                
                ActivityIndicator.stopAnimationWithMessage(strMsg: "Data Updated", completion: {})
            })
        }
    }
    
}

//MARK:- Tableview Datasource and Delegate methods
extension SchoolsListTVC{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSchoolData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NYSchoolCell
        
        let dict = filteredSchoolData[indexPath.row]
        cell.schoolName.text = dict.schoolName
        cell.borough.text = (dict.city ?? "") + ", " + (dict.stateCode ?? "")
        cell.phone.text = "Ph: " + (dict.phoneNumber ?? "-")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailView = storyboard?.instantiateViewController(withIdentifier: "SchoolDetailsTVC") as! SchoolDetailsTVC
        detailView.schoolList = schoolsData
        detailView.schoolResults = satData
        detailView.index = indexPath.row
        
        self.navigationController?.pushViewController(detailView, animated: true)
        
    }
    
    
}

//MARK:- UISearchResultsUpdating
extension SchoolsListTVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard  let searchText = searchController.searchBar.text, searchText != "" else {            filteredSchoolData = schoolsData
            self.tableView.reloadData()
            return
        }
        
        filteredSchoolData = schoolsData.filter({$0.schoolName?.range(of: searchText, options: [.diacriticInsensitive,.caseInsensitive]) != nil})
        self.tableView.reloadData()

        
    }
}



