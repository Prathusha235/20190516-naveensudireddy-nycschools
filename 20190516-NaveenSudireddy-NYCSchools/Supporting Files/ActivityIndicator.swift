//
//  ActivityIndicator.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import UIKit

var indicator = UIActivityIndicatorView()
var greyview = UIView()
var lblMsg = UILabel()

class ActivityIndicator: NSObject {
    
    
    class func showAnimation(strMsg:NSString) -> Void {
        
        DispatchQueue.main.async {
            if indicator.isAnimating{
                self.stopAnimation()
                self.showAnimation(strMsg: strMsg)
                return
            }
            
            
            guard let window = UIApplication.shared.keyWindow else{return }
            window.isUserInteractionEnabled = false
            
            let frame = CGRect(x: window.frame.size.width/2 - 125, y: window.frame.size.height/2 - 60 , width: 250 , height: 100)
            greyview = UIView.init(frame: frame)
            greyview.autoresizingMask=[.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
            
            greyview.backgroundColor=UIColor.black
            greyview.alpha=0.8
            greyview.layer.cornerRadius = 20.0
            
            indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            indicator.autoresizingMask=[.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
            
            let transform: CGAffineTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            indicator.transform = transform
            
            indicator.layer.cornerRadius=3.0
            indicator.color=UIColor.white//init(red: 62/255, green: 152/255, blue: 207/255, alpha: 1.0)
            
            indicator.center = CGPoint(x: 125, y: 40)
            
            
            indicator.hidesWhenStopped = true
            indicator.startAnimating()
            let lblSize: CGSize = strMsg.size(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0)])
            
            lblMsg=UILabel.init(frame: CGRect(x: 250/2 - lblSize.width/2, y: 65, width:lblSize.width, height: 40))
            lblMsg.autoresizingMask=[.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
            
            lblMsg.font=UIFont.boldSystemFont(ofSize: 15)
            lblMsg.text=strMsg as String
            lblMsg.textColor=UIColor.white
            
            greyview.addSubview(indicator)
            greyview.addSubview(lblMsg)
            
            window.addSubview(greyview)
            
        }
    }
    
    
    
    class func stopAnimation() -> Void {
        
        DispatchQueue.main.async {
            guard let window = UIApplication.shared.keyWindow else{return }
            window.isUserInteractionEnabled = true
            
            indicator.stopAnimating()
            indicator.hidesWhenStopped=true
            greyview.removeFromSuperview()
        }
    }
    
    class func stopAnimationWithMessage(strMsg:NSString, completion:@escaping () -> Void) {
        DispatchQueue.main.async {
            
            guard let window = UIApplication.shared.keyWindow else{return }
            window.isUserInteractionEnabled = true
            
            indicator.stopAnimating()
            indicator.hidesWhenStopped=true
            lblMsg.removeFromSuperview()
            
            
            let lblSize1: CGSize = "Success!".size(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20.0)])
            let lblMsg1 = UILabel.init(frame: CGRect(x: 250/2 - lblSize1.width/2, y: 18, width:lblSize1.width, height: 30))
            lblMsg1.autoresizingMask=[.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
            lblMsg1.font=UIFont.boldSystemFont(ofSize: 20)
            lblMsg1.text="Success!"
            lblMsg1.textColor=UIColor.white
            greyview.addSubview(lblMsg1)
            
            let lblSize2: CGSize = strMsg.size(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0)])
            let lblMsg2=UILabel.init(frame: CGRect(x: 250/2 - lblSize2.width/2, y: 55, width:lblSize2.width, height: 40))
            lblMsg2.autoresizingMask=[.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
            lblMsg2.font=UIFont.boldSystemFont(ofSize: 15)
            lblMsg2.text=strMsg as String
            lblMsg2.textColor=UIColor.white
            
            greyview.addSubview(lblMsg2)
            
            delay(1.0, closure: {
                greyview.removeFromSuperview()
                completion()
            })
            
        }
    }
    
    class func removeView() -> Void {
        
        DispatchQueue.main.async {
            greyview.removeFromSuperview()
        }
        
    }
    
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
}
